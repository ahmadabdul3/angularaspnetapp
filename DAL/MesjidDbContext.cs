﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MesjidCommittee.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MesjidCommittee.DAL
{

   
    public class MesjidDbContext : DbContext
    {
        public DbSet<CommunityMember> Member { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}