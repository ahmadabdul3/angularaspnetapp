﻿var mesjidApp = angular.module("mesjidApp", []);

/*mesjidApp.config(function ($routeProvider) {
    $routeProvider
        .when('/AddMember', {
            templateUrl: '~/Views/Members/AddMember.cshtml',
            controller: 'mesjidIndexVM'
        });
});*/

mesjidApp.controller('mesjidIndexVM', function mesjidIndexVM($scope, $http, $compile) {
    $scope.searchText = '';
    $scope.firstName = null;
    $scope.lastName = null;
    $scope.phoneNumber = null;
    $scope.email = null;
    $scope.middleName = null;
    $scope.gender = null;
    $scope.dateOfBirth = null;
    $scope.street1 = null;
    $scope.street2 = null;
    $scope.city = null;
    $scope.state = null;
    $scope.zipCode = null;
    $scope.age = null;
    $scope.numIndInFam = null;
    $scope.numChild = null;

    $scope.fullName = function () {
        return $scope.firstName + ' ' + $scope.lastName;
    };
    $scope.clearPersonData = function clearPersonData() {
        $scope.searchText = null;
        $scope.person.FirstName = null;
        $scope.person.LastName = null;
        $scope.person.PhoneNumber = null;
        $scope.person.Email = null;
        $scope.person.MiddleName = null;
        $scope.person.Gender = null;
        $scope.person.DateOfBirth = null;
        $scope.person.Street1 = null;
        $scope.person.Street2 = null;
        $scope.person.City = null;
        $scope.person.State = null;
        $scope.person.ZipCode = null;
        $scope.person.Age = null;
        $scope.person.NumIndInFam = null;
        $scope.person.NumChild = null;
    }
    $scope.person = function () {
        this.CommunityMemberID = 0;
        this.FirstName = '';
        this.PhoneNumber = '';
        this.Email = '';
        this.MiddleName = '';
        this.LastName = '';
        this.Gender = '';
        this.DateOfBirth = '';
        this.Street1 = '';
        this.Street2 = '';
        this.city = '';
        this.State = '';
        this.ZipCode = '';
        this.Age = '';
        this.NumberOfIndividualsInFamily = '';
        this.NumberOfChildren = '';
    };
    getFirst = function () {
        return $scope.person.FirstName;
    }
    $scope.membersList = [];
    $scope.assignMembersList = function(list) {
        $scope.membersList = list;
    }
    $scope.getMembersList = function () {
        $http.get("/Members/getMembersList")
        .success(function (data) {
            $scope.membersList = data;
        }).error(function (data) {
            console.log(data);
        });
    };
    $scope.editMember = function (memberId) {
        $http({
            url: '/Members/Edit',
            method: "GET",
            params: { id: memberId }
        }).success(function (data) {
            var compiled = $compile(data)($scope);
            $scope.person = getMember(memberId);
            $('#edit-panel-box').html(compiled);
            $('#right-edit-panel').animate({
                width: 500
            }, 200);
        }).error(function (data) {
            console.log(data);
        });
    };
    function getMember(memberId) {
        $http({
            url: '/Members/GetMember',
            method: "GET",
            params: { id: memberId }
        }).success(function (data) {
            $scope.person = data;
        }).error(function (data) {
            console.log(data);
        });
    }
    $scope.saveMember = function () {
        var form = $("#add-member-form").children().first();
        form.removeData('validator');
        form.removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse(form);
        if ($("#add-member-form").children().first().valid()) {
            $http({
                url: '/Members/AddMember',
                method: "POST",
                params: $scope.person
            }).success(function (data) {
                $scope.membersList.push(data);
                $scope.clearPersonData();
                hideModals();
            }).error(function (data) {
                console.log(data);
            });
        }
    };
    $scope.getAddMemberPartial = function () {
        $http.get("/Members/AddMember")
        .success(function (data) {
            var compiled = $compile(data)($scope);
            $('#add-member-div').html(compiled);
            showModal();
            showModalBg();
        }).error(function (data) {
            console.log(data);
        });
    };

});