﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MesjidCommittee.Models
//add fields
    /*
     * what programs they're involved in (sunday school, dar al quran, girls club, playgroup)
     * 
     * 
     * */
{
    public class CommunityMember
    {
        public int CommunityMemberID { get; set; }
        [Display(Name = "First")]
        [Required(ErrorMessage="Required")]
        public string FirstName { get; set; }
        [Display(Name = "Phone")]
        public int? PhoneNumber {get; set;}
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Middle")]
        public string MiddleName { get; set; }
        [Display(Name = "Last")]
        [Required(ErrorMessage = "Required")]
        public string LastName { get; set; }
        public string Gender { get; set; }
        [Display(Name = "Date of birth")]
        public DateTime? DateOfBirth { get; set; }
        [Display(Name = "Street 1")]
        public string Street1 { get; set; }
        [Display(Name = "Street 2")]
        public string Street2 { get; set; }

        public string City { get; set; }
        public string State { get; set; }
        [Display(Name = "Zip")]
        public int? ZipCode { get; set; }
        public int? Age { get; set; }
        public int? NumberOfIndividualsInFamily { get; set; }
        public int? NumberOfChildren { get; set; }


    }
}