﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MesjidCommittee.Models;
using MesjidCommittee.DAL;

namespace MesjidCommittee.Controllers
{
    public class MembersController : Controller
    {
        private MesjidDbContext db = new MesjidDbContext();

        //
        // GET: /Members/

        public ActionResult Index()
        {
            var members = db.Member.OrderBy(x => x.FirstName).ToList();
            return View(members);
        }
        public ActionResult getMembersList()
        {
            var members = db.Member.OrderBy(x => x.FirstName).ToList();
            return Json(members, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Members/Details/5

        public ActionResult Details(int id = 0)
        {
            CommunityMember communitymember = db.Member.Find(id);
            if (communitymember == null)
            {
                return HttpNotFound();
            }
            return View(communitymember);
        }

        //
        // GET: /Members/Create
        [HttpGet]
        public PartialViewResult AddMember()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult AddMember(CommunityMember cMemb)
        {
            /*ModelState.Clear();
            ValidateModel(cMemb);*/
            if (ModelState.IsValid)
            {
                db.Member.Add(cMemb);
                db.SaveChanges();
                return Json(cMemb);
            }
            string error = "Please ensure all required fields have a value";
            return Json(error);
        }
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Members/Create

        [HttpPost]
        public ActionResult Create(CommunityMember communitymember)
        {
            if (ModelState.IsValid)
            {
                db.Member.Add(communitymember);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(communitymember);
        }

        //
        // GET: /Members/Edit/5

        public ActionResult Edit(int id)
        {
            CommunityMember communitymember = db.Member.Find(id);
            if (communitymember == null)
            {
                return HttpNotFound();
            }
            return PartialView(communitymember);
        }
        public ActionResult GetMember(int id)
        {
            CommunityMember communitymember = db.Member.Find(id);
            if (communitymember == null)
            {
                return HttpNotFound();
            }
            return Json(communitymember, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Members/Edit/5

        [HttpPost]
        public ActionResult Edit(CommunityMember communitymember)
        {
            if (ModelState.IsValid)
            {
                db.Entry(communitymember).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(communitymember);
        }

        //
        // GET: /Members/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CommunityMember communitymember = db.Member.Find(id);
            if (communitymember == null)
            {
                return HttpNotFound();
            }
            return View(communitymember);
        }

        //
        // POST: /Members/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            CommunityMember communitymember = db.Member.Find(id);
            db.Member.Remove(communitymember);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}